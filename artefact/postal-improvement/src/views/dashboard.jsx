import React from 'react'
import { connect } from 'react-redux'
import {
  Link,
  Redirect
} from 'react-router-dom'
import AppBar from 'material-ui/AppBar';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import AgentDashboard from './agent/dashboard.jsx'
import DeliveryDashboard from './delivery/dashboard.jsx'
import ReceiverDashboard from './receiver/dashboard.jsx'
import AdminDashboard from './admin/dashboard.jsx'
import FlatButton from 'material-ui/FlatButton'

const PORTAL_NAMES = {
  AGENT : "agent",
  DELIVERY : "deliverer",
  RECEIVER: "receiver",
  ADMIN: "admin"
}

class Dashboard extends React.Component {

  constructor( props ) {
    super( props )
    console.log(this.props)
    this.state = {
      activePortal: props.user.data && Object.keys(props.user.data.roles)[0]
    }
  }

  setPortal = ( portalName ) => {
    return () => {
      this.setState( ( prevState, props ) => ( {
        activePortal: portalName
      } ) )
    }
  }

  render () {
    return this.props.user.data ? (
      <div key="dashboard" id="dashboard">
        <AppBar
          iconElementRight={<Link to="/login"><FlatButton label="logout" /></Link>}
          title={`${this.state.activePortal} Dashboard`} />
        <Menu>
          {Object.keys( this.props.user.data.roles )
            .map((roleName) => <MenuItem
              key={roleName}
              onClick={this.setPortal(roleName)}
              primaryText={`${roleName} portal`} />)
          }
        </Menu>
        {
          this.state.activePortal === PORTAL_NAMES.AGENT &&
          <AgentDashboard />
        }
        {
          this.state.activePortal === PORTAL_NAMES.DELIVERY &&
          <DeliveryDashboard />
        }
        {
          this.state.activePortal === PORTAL_NAMES.RECEIVER &&
          <ReceiverDashboard />
        }
        {
          this.state.activePortal === PORTAL_NAMES.ADMIN &&
          <AdminDashboard />
        }
      </div>
    ) :
    (<Redirect to="/" />)
  }
}

const mapStateToProps = ( {user, ...state} ) => {
  return {user}
}

export default connect(mapStateToProps)(Dashboard)

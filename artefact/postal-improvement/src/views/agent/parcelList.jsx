import React from 'react'
import { List, ListItem } from 'material-ui/List'
import Subheader from 'material-ui/Subheader';
import { DetailList } from './detailList.jsx'
import RaisedButton from 'material-ui/RaisedButton'
import TextField from 'material-ui/TextField';
import Dialog from 'material-ui/Dialog';

export default class ParcelList extends React.Component {
  newParcel = () => {
    this.handleClose()
  }

  state = {
    open: false,
    code: ""
  };

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    console.log(this.state.code)
    this.setState({open: false});
  };

  actions = [
    <RaisedButton onClick={this.newParcel} label="create" />
  ]

  updatePIDCode = ( e ) => {
    this.setState( {
      code: e.target.value
    })
  }

  render () {
    return (
      <div>
        <RaisedButton onClick={this.handleOpen} label="new parcel" />
        <Dialog
          title="receive new parcels"
          actions={this.actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}>
          provide identification code of the parcel to receive
          <br />
          <TextField
            onChange={this.updatePIDCode}
            hintText="parcel identification code" />
        </Dialog>
        <List>
          <Subheader>
            <h1>parcel list</h1>
          </Subheader>
          {this.props.data && this.props.data.map( ( item ) =>
            <ListItem
              key={item.trackNo}
              primaryTogglesNestedList={true}
              nestedItems={DetailList(item)}
              primaryText={`${item.trackNo} ----- ${item.destination}`}>
            </ListItem>
          )}
        </List>
      </div>
    )
  }
}

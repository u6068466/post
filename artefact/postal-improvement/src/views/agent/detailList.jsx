import React from 'react'
import { ListItem } from 'material-ui/List'

export const DetailList = ( item ) => {
  return Object.keys( item ).map( ( key ) =>  <ListItem key={key} primaryText={`${key} : ${item[key]}`} />)
}

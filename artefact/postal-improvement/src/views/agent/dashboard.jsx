import React from 'react'
import ParcelList from './parcelList.jsx'
import { connect } from 'react-redux'
import { parcelApi } from '../../apis/parcel/api'
import { parcelActions } from '../../store/actions/parcelAction'

class AgentDashboard extends React.Component {

  onReadParcelFail = () => {
    console.log("read parcel fail")
  }

  onReadParcelSuccess = (parcels) => {
    console.log( "read parcel success" )
    console.log( parcels )
    this.props.setAgentParcelList( parcels )
  }

  fetchAgentParcelList = () => {
    const conditions = {
      holderID: this.props.user.data.id
    }
    parcelApi.read(conditions, this.onReadParcelSuccess, this.onReadParcelFail)
  }

  componentDidMount () {
    this.fetchAgentParcelList()
  }

  onUpdate = () => {
    this.fetchAgentParcelList()
  }

  render () {
    console.log(this.props)
    return <ParcelList data={this.props.parcel.agentParcelList} onUpdate={this.onUpdate} />
  }
}

const mapStateToProps = ( {user, parcel, ...state} ) => {
  return {
    user,
    parcel
  }
}

const mapDispatchToProps = ( dispatch ) => ( {
  setAgentParcelList: (parcelList) => dispatch(parcelActions.setParcelForAgent(parcelList))
})

export default connect(mapStateToProps, mapDispatchToProps)(AgentDashboard)

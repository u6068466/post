import React from 'react'
import { List, ListItem } from 'material-ui/List'
import Subheader from 'material-ui/Subheader'
import { DetailList } from './detailList.jsx'
import RaisedButton from 'material-ui/RaisedButton'
import { parcelApi } from '../../apis/parcel/api'
import { deliveryApi } from '../../apis/delivery/api'
import { guid } from '../../utils'

export default class ParcelList extends React.Component {

  onFail = () => {
    console.log("parcel api update failed")
  }

  setSelfCollect = ( item ) => ( e ) => {
    e.stopPropagation()
    const params = {
      item: {id:item.id},
      query: {
        collect: "pickup"
      }
    }
    parcelApi.update( params, this.props.onUpdate, this.onFail )

    const deleteParams = {
      parcelID: item.id
    }
    deliveryApi.delete( deleteParams, () => { }, () => { } )
  }

  deliverNow = ( item ) => (e) => {
    e.stopPropagation()
    const params = {
      item: {id:item.id},
      query: {
        collect: "deliver"
      }
    }
    parcelApi.update( params, this.props.onUpdate, this.onFail )

    const defaultTime = new Date()
    const hour = defaultTime.getHours()
    defaultTime.setHours(hour+3)
    const newDelivery = {
      id: guid(),
      parcelID: item.id,
      status: "waiting",
      before: defaultTime,
      isRecordActive: true,
      meta: {
        createdAt: new Date(),
        createdBy: this.props.user.data.id
      }
    }
    deliveryApi.create(newDelivery, ()=>{},()=>{})
  }

  render () {
    return (
      <div>
        <List>
          <Subheader>
            <h1>parcel list</h1>
          </Subheader>
          {this.props.data && this.props.data.map( ( item ) =>
            <ListItem
              key={item.trackNo}
              primaryTogglesNestedList={true}
              nestedItems={DetailList(item)}>
              {`${item.trackNo} ----- ${item.collect}`}
              <br />
              <br />
              <RaisedButton
                key="1"
                onClick={this.setSelfCollect(item)}
                label="self collect" />
              <RaisedButton
                key="2"
                onClick={this.deliverNow(item)}
                label="deliver now (within 3 hours)" />
            </ListItem>
          )}
        </List>
      </div>
    )
  }
}

import React from 'react'
import ParcelList from './parcelList.jsx'
import { connect } from 'react-redux'
import { parcelApi } from '../../apis/parcel/api'
import { parcelActions } from '../../store/actions/parcelAction'
import Dialog from 'material-ui/Dialog';

class ReceiverDashboard extends React.Component {

  onReadParcelFail = () => {
    console.log("read parcel fail")
  }

  onReadParcelSuccess = (parcels) => {
    console.log( "read parcel success" )
    console.log( parcels )
    this.props.setReceiverParcelList( parcels )
  }

  fetchReceiverParcelList = () => {
    const conditions = {
      receiverName: this.props.user.data.name,
      receiverEmail: this.props.user.data.email
    }
    parcelApi.read(conditions, this.onReadParcelSuccess, this.onReadParcelFail)
  }

  componentDidMount () {
    this.fetchReceiverParcelList()
  }

  onUpdate = (data) => {
    this.fetchReceiverParcelList()
  }

  render () {
    console.log(this.props)
    return <ParcelList
      user={this.props.user}
      data={this.props.parcel.receiverParcelList}
      onUpdate={this.onUpdate} />
  }
}

const mapStateToProps = ( {user, parcel, ...state} ) => {
  return {
    user,
    parcel
  }
}

const mapDispatchToProps = ( dispatch ) => ( {
  setReceiverParcelList: (parcelList) => dispatch(parcelActions.setParcelForReceiver(parcelList))
})

export default connect(mapStateToProps, mapDispatchToProps)(ReceiverDashboard)

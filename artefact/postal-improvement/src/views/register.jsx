import React from 'react'
import RaisedButton from 'material-ui/RaisedButton'
import TextFields from 'material-ui/TextField'
import SelectField from 'material-ui/SelectField'
import AppBar from 'material-ui/AppBar'
import MenuItem from 'material-ui/MenuItem'
import {userApi} from '../apis/user/api'
import { Redirect } from 'react-router-dom'

export default class RegistrPage extends React.Component {
  state = {
    data: {
      name: "",
      password: "",
      email: "",
      address: "",
      roles: {}
    },
    registerSuccess: false
  }

  updateData = ( newData ) => {
    const nextState = {
      ...( this.state ),
      data: {
        ...( this.state.data ),
        ...newData
      }
    }
    this.setState( nextState )
  }

  updateRegisterStatus = (registerStatus) => {
    this.setState( {
      ...( this.state ),
      ...registerStatus
    })
  }

  onCreateUserSuccess = () => {
    console.log( "create user success" )
    this.updateRegisterStatus( {
      registerSuccess: true
    } )
    console.log(this.state)
  }

  onCreateUserFail = () => {
    console.log("create user fail")
  }

  register = () => {
    userApi.create(this.state.data, this.onCreateUserSuccess, this.onCreateUserFail)
  }

  render () {
    return this.state.registerSuccess ?
    (<Redirect to="/dashboard" />) :
    (
      <div id="register">
        <AppBar title="Register Page" />
        <br />
        <TextFields
          id="register-username"
          hintText="user name"
          onChange={( e ) => this.updateData( { name: e.target.value } )} />
        <br />
        <TextFields
          id="register-password"
          hintText="password"
          onChange={( e ) => this.updateData( { password: e.target.value } )}
          type="password" />
        <br />
        <TextFields
          id="register-email"
          hintText="email"
          onChange={( e ) => this.updateData( { email: e.target.value } )} />
        <br />
        <TextFields
          id="register-address"
          hintText="address"
          onChange={( e ) => this.updateData( { address: e.target.value } )} />
        <br />
        <SelectField
          id="register-roles"
          floatingLabelText="select your roles"
          value={Object.keys(this.state.data.roles)}
          multiple={true}
          onChange={( event, index, values ) => {
            var newRoles = {}
            values.forEach(value => {
              newRoles[value] = true
            } )
            var nextData = {
              ...( this.state.data ),
              roles: newRoles
            }
            this.updateData( nextData )
          }}
        >
          <MenuItem value="agent" primaryText="agent" />
          <MenuItem value="receiver" primaryText="receiver" />
          <MenuItem value="deliverer" primaryText="deliverer" />
          <MenuItem value="admin" primaryText="admin" />
        </SelectField>
        <br />
        <RaisedButton label="register" onClick={this.register} />
      </div>
    )
  }
}

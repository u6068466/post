import React from 'react'
import { ListItem } from 'material-ui/List'
import RaisedButton from 'material-ui/RaisedButton'
import { deliveryApi } from '../../apis/delivery/api'
import QRCode from 'qrcode.react'

export const DetailList = ( user, item, onUpdate ) => {
  const onFail = () => {
    console.log("update delivery fail")
  }

  const startDelivery = ( ) => {
    const params = {
      item: {id:item.id},
      query: {
        status: "delivering",
        delivererID: user.data.id
      }
    }
    deliveryApi.update( params, onUpdate, onFail)
  }

  const finishDelivery = ( ) => {
    const params = {
      item: {id:item.id},
      query: {
        status: "finished",
      }
    }
    deliveryApi.update( params, onUpdate, onFail)
  }

  var list = Object.keys( item ).map( ( key ) => <ListItem key={key} primaryText={`${key} : ${item[ key ]}`} /> )
  if ( item.status === "waiting" ) {
    list.push(
      <RaisedButton
        key="start"
        onClick={startDelivery}
        label="start delivery" />
    )
  } else {
    list.push(
      <RaisedButton
        key="finish"
        onClick={finishDelivery}
        label="finish delivery" />
    )
  }
  return list
}

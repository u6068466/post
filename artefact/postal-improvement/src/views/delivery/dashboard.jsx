import React from 'react'
import DeliveryList from './deliveryList.jsx'
import { connect } from 'react-redux'
import { deliveryApi } from '../../apis/delivery/api'
import { deliveryActions } from '../../store/actions/deliveryAction'

class DeliveryDashboard extends React.Component {

  fetchMyDeliveryList = () => {
    const conditions = {
      status: "delivering",
      delivererID: this.props.user.data.id
    }
    deliveryApi.read( conditions, ( deliveryList ) => {
      console.log("fetch my delivery list success", deliveryList)
      this.props.setMyDeliveryList(deliveryList)
    }, () => {
      console.log("fetch my delivery list fail")
    })
  }

  fetchAvailableDeliveryList = () => {
    const conditions = {
      status: "waiting"
    }
    deliveryApi.read( conditions, ( deliveryList ) => {
      console.log("fetch available delivery list success", deliveryList)
      this.props.setAvailableDeliveryList(deliveryList)
    }, () => {
      console.log("fetch available delivery list fail")
    })
  }

  componentDidMount () {
    this.fetchMyDeliveryList()
    this.fetchAvailableDeliveryList()
  }

  onUpdate = () => {
    this.fetchMyDeliveryList()
    this.fetchAvailableDeliveryList()
  }

  render () {
    return (
      <div>
        <DeliveryList
          title="available deliveries"
          user={this.props.user}
          data={this.props.delivery && this.props.delivery.availableDeliveryList}
          onUpdate={this.onUpdate} />
        <DeliveryList
          title="my deliveries"
          user={this.props.user}
          data={this.props.delivery && this.props.delivery.myDeliveryList}
          onUpdate={this.onUpdate} />
      </div>
    )
  }
}

const mapStateToProps = ( {user, delivery, ...state} ) => {
  return {
    user,
    delivery
  }
}

const mapDispatchToProps = ( dispatch ) => ( {
  setMyDeliveryList: (deliveryList) => dispatch(deliveryActions.setMyDeliveryList(deliveryList)),
  setAvailableDeliveryList: (deliveryList) => dispatch(deliveryActions.setAvailableDeliveryList(deliveryList))
})

export default connect(mapStateToProps, mapDispatchToProps)(DeliveryDashboard)

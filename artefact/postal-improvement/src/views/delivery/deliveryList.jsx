import React from 'react'
import { List, ListItem } from 'material-ui/List'
import Subheader from 'material-ui/Subheader'
import { DetailList } from './detailList.jsx'

export default class DeliveryList extends React.Component {
  render () {
    return (
      <div>
        <List>
          <Subheader>
            <h1>{this.props.title}</h1>
          </Subheader>
          {this.props.data && this.props.data.map( ( item ) =>
            <ListItem
              key={item.id}
              primaryTogglesNestedList={true}
              nestedItems={DetailList(this.props.user, item, this.props.onUpdate)}
              primaryText={`${item.parcelID} ----- ${item.before}`}>
            </ListItem>
          )}
        </List>
      </div>
    )
  }
}

import React from 'react'
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton'
import { Link } from "react-router-dom";

export default class Home extends React.Component {

  render () {
    return (
      <div id="home">
        <AppBar title="Home Page" />
        <br />
        <Link to="/login">
          <RaisedButton label="login" />
        </Link>
        <br />
        <br />
        <Link to="/register">
          <RaisedButton label="register" />
        </Link>
      </div>
    )
  }
}

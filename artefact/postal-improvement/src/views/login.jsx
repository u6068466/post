import React from 'react'
import RaisedButton from 'material-ui/RaisedButton'
import TextFields from 'material-ui/TextField'
import AppBar from 'material-ui/AppBar'
import { userApi } from '../apis/user/api'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { userActions } from '../store/actions/userAction'

class LoginPage extends React.Component {
  state = {
    data: {
      name: "",
      password: ""
    },
    loginSuccess: false
  }

  updateData = ( newData ) => {
    const nextState = {
      ...( this.state ),
      data: {
        ...( this.state.data ),
        ...newData
      }
    }
    this.setState( nextState )
  }

  updateLoginStatus = (loginStatus) => {
    this.setState( {
      ...( this.state ),
      ...loginStatus
    })
  }

  onReadUserSuccess = ( data ) => {
    console.log("read user success")
    if ( data.length === 0 ) {
      console.log("user does not exist")
    } else {
      console.log( "user exists" )
      this.props.setUser(data[0])
      this.updateLoginStatus( {
        loginSuccess: true
      } )
    }
  }

  onReadUserFail = () => {
    console.log("read user fail")
  }

  login = () => {
    userApi.read(this.state.data, this.onReadUserSuccess, this.onReadUserFail)
  }

  render () {
    return this.state.loginSuccess ?
    (<Redirect to="/dashboard" />):
    (
      <div key="login" id="login">
        <AppBar title="Login Page" />
        <br />
        <TextFields
          id="login-username"
          hintText="user name"
          onChange={( e ) => this.updateData( { name: e.target.value } )} />
        <br />
        <TextFields
          id="login-password"
          hintText="password"
          onChange={( e ) => this.updateData( { password: e.target.value } )}
          type="password" />
        <br />
        <RaisedButton label="login" onClick={this.login} />
      </div>
    )
  }
}

const mapStateToProps = ( state ) => ( {

})

const mapDispatchToProps = ( dispatch ) => ( {
  setUser: (user) => dispatch(userActions.set(user))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage)

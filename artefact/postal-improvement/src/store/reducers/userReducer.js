import { handleActions } from 'redux-actions'
import { actionTypes } from '../actions/userAction'

const initialState = {
}

const userReducer = handleActions( {
  [ actionTypes.set ]: ( user, action ) => {
    console.log("reduce set user", user, action)
    return {
      ...user,
      data: action.payload.user
    }
  }
}, initialState)

export default userReducer

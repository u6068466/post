import { handleActions } from 'redux-actions'
import { actionTypes } from '../actions/parcelAction'

const initialState = {
}

const parcelReducer = handleActions( {
  [ actionTypes.setParcelForReceiver ]: ( parcel, action ) => {
    return {
      ...parcel,
      receiverParcelList: action.payload.receiverParcelList
    }
  },
  [ actionTypes.setParcelForDeliverer ]: ( parcel, action ) => {
    return {
      ...parcel,
      delivererParcelList: action.payload.delivererParcelList
    }
  },
  [ actionTypes.setParcelForAgent ]: ( parcel, action ) => {
    return {
      ...parcel,
      agentParcelList: action.payload.agentParcelList
    }
  },
}, initialState)

export default parcelReducer

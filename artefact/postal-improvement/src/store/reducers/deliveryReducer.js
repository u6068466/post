import { handleActions } from 'redux-actions'
import { actionTypes } from '../actions/deliveryAction'

const initialState = {
}

const deliveryReducer = handleActions( {
  [ actionTypes.setMyDeliveryList ]: ( delivery, action ) => {
    return {
      ...delivery,
      myDeliveryList: action.payload.myDeliveryList
    }
  },
  [ actionTypes.setAvailableDeliveryList ]: ( delivery, action ) => {
    return {
      ...delivery,
      availableDeliveryList: action.payload.availableDeliveryList
    }
  },
}, initialState)

export default deliveryReducer

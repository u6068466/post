import { createAction } from 'redux-actions'

export const actionTypes = {
  setMyDeliveryList: 'SET_MY_DELIVERY_LIST',
  setAvailableDeliveryList: 'SET_AVAILABLE_DELIVERY_LIST',
}

export const deliveryActions = {
  setMyDeliveryList: createAction(actionTypes.setMyDeliveryList, (myDeliveryList)=>({myDeliveryList})),
  setAvailableDeliveryList: createAction(actionTypes.setAvailableDeliveryList, (availableDeliveryList)=>({availableDeliveryList})),
}

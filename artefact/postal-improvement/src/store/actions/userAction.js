import { createAction } from 'redux-actions'

export const actionTypes = {
  set: 'USER_SET'
}

export const userActions = {
  set: createAction(actionTypes.set, (user)=>({user}))
}

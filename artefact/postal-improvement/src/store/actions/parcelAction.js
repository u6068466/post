import { createAction } from 'redux-actions'

export const actionTypes = {
  setParcelForReceiver: 'SET_PARCEL_FOR_RECEIVER',
  setParcelForDeliverer: 'SET_PARCEL_FOR_DELIVERER',
  setParcelForAgent: 'SET_PARCEL_FOR_AGENT'
}

export const parcelActions = {
  setParcelForReceiver: createAction(actionTypes.setParcelForReceiver, (receiverParcelList)=>({receiverParcelList})),
  setParcelForDeliverer: createAction(actionTypes.setParcelForDeliverer, (delivererParcelList)=>({delivererParcelList})),
  setParcelForAgent: createAction(actionTypes.setParcelForAgent, (agentParcelList)=>({agentParcelList}))
}

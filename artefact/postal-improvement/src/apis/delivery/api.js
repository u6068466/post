import {commonApi} from '../commonApi'

const modelName = 'delivery'

export const deliveryApi = {
  create: commonApi.create( modelName ),
  read: commonApi.read( modelName ),
  update: commonApi.update( modelName ),
  delete: commonApi.delete( modelName )
}

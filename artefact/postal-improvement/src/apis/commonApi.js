import axios from 'axios'

const host = 'http://localhost:8000'

export const commonApi = {
  create: ( modelName ) => ( params, success, fail ) => {
    axios.post( `${host}/${modelName}/create`, params ).then( ( res ) => {
      if ( res.status === 200 ) {
        success()
      } else {
        fail()
      }
    } ).catch( ( err ) => {
      console.log( err );
    })
  },
  read: ( modelName ) => ( params, success, fail ) => {
    axios.post( `${host}/${modelName}/read`, params ).then( ( res ) => {
      if ( res.status === 200 ) {
        success(res.data)
      } else {
        fail()
      }
    } ).catch( ( err ) => {
      console.log( err );
    })
  },
  update: ( modelName ) => ( params, success, fail ) => {
    console.log("update database", params)
    axios.post( `${host}/${modelName}/update`, params ).then( ( res ) => {
      if ( res.status === 200 ) {
        success(res)
      } else {
        fail()
      }
    } ).catch( ( err ) => {
      console.log( err );
    })
  },
  delete: ( modelName ) => ( params, success, fail ) => {
    axios.post( `${host}/${modelName}/delete`, params ).then( ( res ) => {
      if ( res.status === 200 ) {
        success()
      } else {
        fail()
      }
    } ).catch( ( err ) => {
      console.log( err );
    })
  },
}

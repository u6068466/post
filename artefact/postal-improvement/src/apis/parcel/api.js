import {commonApi} from '../commonApi'

const modelName = 'parcel'

export const parcelApi = {
  create: commonApi.create( modelName ),
  read: commonApi.read( modelName ),
  update: commonApi.update( modelName ),
  delete: commonApi.delete( modelName )
}

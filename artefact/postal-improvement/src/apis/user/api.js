import {commonApi} from '../commonApi'

const modelName = 'user'

export const userApi = {
  create: commonApi.create( modelName ),
  read: commonApi.read( modelName ),
  update: commonApi.update( modelName ),
  delete: commonApi.delete( modelName )
}

import React from 'react'
import { BrowserRouter as Router, Route } from "react-router-dom";
import Home from '../views/home'
import Login from '../views/login'
import Register from '../views/register'
import Dashboard from '../views/dashboard'

export default () =>
  <Router>
    <div>
      <Route path="/" component={Home} exact />
      <Route path="/login" component={Login} exact />
      <Route path="/register" component={Register} exact />
      <Route path="/dashboard" component={Dashboard} exact />
    </div>
  </Router>

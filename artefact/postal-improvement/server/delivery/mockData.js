const mockDate = new Date()
hour = mockDate.getHours()
mockDate.setHours(hour + 3)

const deliveryList = [
  {
    id: "delivery1",
    parcelID: "parcel1",
    delivererID: "user2",
    status: "delivering",
    before: mockDate,
    isRecordActive: true,
    meta: {
      createdAt: new Date(),
      createdBy: "user1",
      updatedAt: new Date(),
      updatedBy: "user2"
    }
  },
  {
    id: "delivery2",
    parcelID: "parcel2",
    status: "waiting",
    before: mockDate,
    isRecordActive: true,
    meta: {
      createdAt: new Date(),
      createdBy: "user1",
    }
  }
]

module.exports = {
  deliveryList: deliveryList
}

const deliverySchema = {
  id: {
    type: String,
    require
  },
  parcelID: {
    type: String,
    require: true
  },
  delivererID: {
    type: String,
    require: false
  },
  status: {
    type: String,
    require: true
  },
  before: {
    type: Date,
    require: true
  },
  isRecordActive: {
    type: Boolean,
    require: true
  },
  meta: {
    createdAt: {
      type: Date,
      require: false
    },
    updatedAt: {
      type: Date,
      require: false
    },
    deletedAt: {
      type: Date,
      require: false
    },
    createdBy: {
      type: String,
      require: false
    },
    updatedBy: {
      type: String,
      require: false
    },
    deletedBy: {
      type: String,
      require: false
    },
  }
}

module.exports = {
  deliverySchema: deliverySchema
}

const deliverySchema = require("./schema").deliverySchema

const createApi = ( app, mongoose ) => {
  const DeliverySchema = new mongoose.Schema( deliverySchema )

  const Delivery = mongoose.model( 'delivery', DeliverySchema )

  // create
  app.post( '/delivery/create', function ( req, res ) {
    Delivery.create( req.body, ( err, doc ) => {
      if ( !err ) {
        console.log( "delivery created" )
        console.log(doc)
        res.status(200)
        res.send("create delivery success")
      } else {
        res.status(300)
        res.send("create delivery error")
      }
    })
  } )

  // read
  app.post( '/delivery/read', function ( req, res ) {
    Delivery.find( req.body, ( err, doc ) => {
      if ( !err ) {
        res.status(200)
        res.json(doc)
      } else {
        res.status( 300 )
        res.send("delivery read error")
      }
    })
  } )

  // update
  app.post( '/delivery/update', function ( req, res ) {
    Delivery.update( req.body.item, {
      '$set': req.body.query
    }, ( err, doc ) => {
      if ( err ) {
        res.status( 300 )
        res.send("delivery update fail")
      } else {
        res.status(200)
        res.send("delivery update success")
      }
    })
  } )

  // delete
  app.post( '/delivery/delete', function ( req, res ) {
    Delivery.remove( req.body, ( err, doc ) => {
      if ( err ) {
        res.status( 300 )
        res.send("delivery delete fail")
      } else {
        res.status(200)
        res.send("delivery delete success")
      }
    })
  } )
}

module.exports = {
  createApi: createApi
}

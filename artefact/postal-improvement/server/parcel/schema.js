const parcelSchema = {
  id: {
    type: String,
    require
  },
  trackNo: {
    type: Number,
    require: true
  },
  receiverName: {
    type: String,
    require: true
  },
  receiverEmail: {
    type: String,
    require: true
  },
  isRecordActive: {
    type: Boolean,
    require: true
  },
  holderID: {
    type: String,
    require: true
  },
  destination: {
    type: String,
    require: true
  },
  collect: {
    type: String,
    require: true
  },
  meta: {
    createdAt: {
      type: Date,
      require: false
    },
    updatedAt: {
      type: Date,
      require: false
    },
    deletedAt: {
      type: Date,
      require: false
    },
    createdBy: {
      type: String,
      require: false
    },
    updatedBy: {
      type: String,
      require: false
    },
    deletedBy: {
      type: String,
      require: false
    },
  }
};

module.exports = {
  parcelSchema: parcelSchema
};

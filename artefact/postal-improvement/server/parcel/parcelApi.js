const parcelSchema = require("./schema").parcelSchema

const createApi = ( app, mongoose ) => {
  const ParcelSchema = new mongoose.Schema( parcelSchema )

  const Parcel = mongoose.model( 'parcel', ParcelSchema )

  // create
  app.post( '/parcel/create', function ( req, res ) {
    Parcel.create( req.body, ( err, doc ) => {
      if ( !err ) {
        res.status(200)
        res.send("create parcel success")
      } else {
        res.status(300)
        res.send("create parcel error")
      }
    })
  } )

  // read
  app.post( '/parcel/read', function ( req, res ) {
    Parcel.find( req.body, ( err, doc ) => {
      if ( !err ) {
        res.status(200)
        res.json(doc)
      } else {
        res.status( 300 )
        res.send("parcel read error")
      }
    })
  } )

  // update
  app.post( '/parcel/update', function ( req, res ) {
    Parcel.update( req.body.item, {
      '$set': req.body.query
    }, ( err, doc ) => {
      if ( err ) {
        res.status( 300 )
        res.send("parcel update fail")
      } else {
        res.status(200)
        res.send("parcel update success")
      }
    })
  } )

  // delete
  app.post( '/parcel/delete', function ( req, res ) {
    Parcel.remove( req.body, ( err, doc ) => {
      if ( err ) {
        res.status( 300 )
        res.send("parcel delete fail")
      } else {
        res.status(200)
        res.send("parcel delete success")
      }
    })
  } )
}

module.exports = {
  createApi: createApi
}

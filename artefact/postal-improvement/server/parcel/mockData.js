const parcelList = [
  {
    id: "parcel1",
    trackNo: 1,
    receiverName: "muduo",
    receiverEmail: "muduo@test.com",
    isRecordActive: true,
    holderID: "user2",
    destination: "academie house",
    collect: "delivery",
    meta: {
      createdAt: new Date(),
      createdBy: "user3",
      updatedAt: new Date(),
      updatedBy: "user2"
    }
  },
  {
    id: "parcel2",
    trackNo: 2,
    receiverName: "cindy",
    receiverEmail: "cindy@test.com",
    isRecordActive: true,
    holderID: "user3",
    destination: "kinlock",
    collect: "pickup",
    meta: {
      createdAt: new Date(),
      createdBy: "user3"
    }
  },
  {
    id: "parcel3",
    trackNo: 3,
    receiverName: "harrison",
    receiverEmail: "harrison@test.com",
    isRecordActive: true,
    holderID: "user3",
    destination: "academie house",
    collect: "pickup",
    meta: {
      createdAt: new Date(),
      createdBy: "user3"
    }
  },
  {
    id: "parcel4",
    trackNo: 4,
    receiverName: "shawn",
    receiverEmail: "shawn@test.com",
    isRecordActive: true,
    holderID: "user3",
    destination: "academie house",
    collect: "pickup",
    meta: {
      createdAt: new Date(),
      createdBy: "user3"
    }
  },
  {
    id: "parcel5",
    trackNo: 5,
    receiverName: "wendy",
    receiverEmail: "wendy@test.com",
    isRecordActive: true,
    holderID: "user3",
    destination: "toad hall",
    collect: "pickup",
    meta: {
      createdAt: new Date(),
      createdBy: "user3"
    }
  },
  {
    id: "parcel6",
    trackNo: 6,
    receiverName: "ethan",
    receiverEmail: "ethan@test.com",
    isRecordActive: true,
    holderID: "user3",
    destination: "belconnen",
    collect: "pickup",
    meta: {
      createdAt: new Date(),
      createdBy: "user3"
    }
  },
]

module.exports = {
  parcelList: parcelList
}

const userSchema = {
  id: {
    type: String,
    require: true
  },
  name: {
    type: String,
    require: true
  },
  password: {
    type: String,
    require: true
  },
  email: {
    type: String,
    require: true
  },
  address: {
    type: String,
    require: true
  },
  isRecordActive: {
    type: Boolean,
    require: true
  },
  roles: {
    receiver: {
      type: Boolean,
      require: false
    },
    deliverer: {
      type: Boolean,
      require: false
    },
    agent: {
      type: Boolean,
      require: false
    },
    admin: {
      type: Boolean,
      require: false
    }
  },
  credit: {
    totalScore: {
      type: Number,
      require: true
    },
    times: {
      type: Number,
      require: true
    }
  },
  meta: {
    createdAt: {
      type: Date,
      require: false
    },
    updatedAt: {
      type: Date,
      require: false
    },
    deletedAt: {
      type: Date,
      require: false
    },
    createdBy: {
      type: String,
      require: false
    },
    updatedBy: {
      type: String,
      require: false
    },
    deletedBy: {
      type: String,
      require: false
    },
  }
};

module.exports = {
  userSchema: userSchema
};

const userSchema = require("./schema").userSchema

const createApi = ( app, mongoose ) => {
  const UserSchema = new mongoose.Schema( userSchema );

  const User = mongoose.model( 'user', UserSchema );

  // create
  app.post( '/user/create', function ( req, res ) {
    User.create( req.body, ( err, doc ) => {
      if ( !err ) {
        res.status(200)
        res.send("create user success")
      } else {
        res.status(300)
        res.send("create user error")
      }
    })
  } )

  // read
  app.post( '/user/read', function ( req, res ) {
    User.find( req.body, ( err, doc ) => {
      if ( !err ) {
        res.status(200)
        res.json(doc)
      } else {
        res.status( 300 )
        res.send("user login error")
      }
    })
  } )

  // update
  app.post( '/user/update', function ( req, res ) {
    User.update( req.body.item, {
      '$set': req.body.query
    }, ( err, doc ) => {
      if ( err ) {
        res.status( 300 )
        res.send("user update fail")
      } else {
        res.status(200)
        res.send("user update success")
      }
    })
  } )

  // delete
  app.post( '/user/delete', function ( req, res ) {
    User.remove( req.body, ( err, doc ) => {
      if ( err ) {
        res.status( 300 )
        res.send("user delete fail")
      } else {
        res.status(200)
        res.send("user delete success")
      }
    })
  } )
}

module.exports = {
  createApi: createApi
}

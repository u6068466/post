const userList = [
  {
    id: "user1",
    name: "muduo",
    password: "password",
    email: "muduo@test.com",
    address: "Academie House",
    isRecordActive: true,
    roles: {
      receiver: true,
      agent: true,
      deliverer: true,
      admin: true
    },
    credit: {
      totalScore: 100,
      times: 10
    },
    meta: {
      createdAt: new Date(),
      createdBy: "muduo@test.com"
    }
  },
  {
    id: "user2",
    name: "peter",
    password: "password",
    email: "peter@test.com",
    address: "Belconnen",
    isRecordActive: true,
    roles: {
      deliverer: true
    },
    credit: {
      totalScore: 100,
      times: 10
    },
    meta: {
      createdAt: new Date(),
      createdBy: "peter@test.com"
    }
  },
  {
    id: "user3",
    name: "seven eleven",
    password: "password",
    email: "seven-eleven@test.com",
    address: "city",
    isRecordActive: true,
    roles: {
      agent: true
    },
    credit: {
      totalScore: 100,
      times: 10
    },
    meta: {
      createdAt: new Date(),
      createdBy: "seven-eleven@test.com"
    }
  }
]

module.exports = {
  userList: userList
}

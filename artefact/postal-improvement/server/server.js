const express = require('express')
const mongoose = require( 'mongoose' )
const path = require( 'path' )

const app = express()
const cors = require( 'cors' )
const bodyParser = require( 'body-parser' )

const User =  require('./user/userApi')
const Parcel = require('./parcel/parcelApi')
const Delivery = require('./delivery/deliveryApi')

app.use( cors() )
app.use( bodyParser.json() )
app.use( express.static( __dirname + '/../build' ) )

const DB_URL = 'mongodb://localhost:27017/post'
mongoose.connect( DB_URL )
mongoose.connection.on( 'connected', () => {
  console.log( "connect to mongodb successful" )
} )

User.createApi(app, mongoose)
Parcel.createApi(app, mongoose)
Delivery.createApi( app, mongoose )

app.get('*', function (request, response){
  response.sendFile(path.resolve(__dirname, '../build', 'index.html'))
})

const port = 8000
app.listen(port, function(){
  console.log("server is listening on localhost:" + port)
})

const mongoose = require( 'mongoose' )
const userSchema = require('./user/schema').userSchema
const parcelSchema = require('./parcel/schema').parcelSchema
const deliverySchema = require('./delivery/schema').deliverySchema
const userList = require( './user/mockData' ).userList
const parcelList = require( './parcel/mockData' ).parcelList
const deliveryList = require( './delivery/mockData' ).deliveryList


const DB_URL = 'mongodb://localhost:27017/post'
mongoose.connect( DB_URL )
mongoose.connection.on( 'connected', () => {
  console.log( "connect to mongodb successful" )
})

const generateUsers = () => {
  const UserSchema = new mongoose.Schema( userSchema )
  const User = mongoose.model( 'user', UserSchema )
  User.remove( { }, ( err, doc ) => {
    console.log( "*************** removed users ********************" )
    console.log(doc)
  } )

  setTimeout(() => {
    userList.forEach( ( user ) => {
      User.create( user, ( err, doc ) => {
        if ( !err ) {
          console.log( doc )
        } else {
          console.log( err )
        }
      })
    })
  }, 2000 );
}

const generateParcels = () => {
  const ParcelSchema = new mongoose.Schema( parcelSchema )
  const Parcel = mongoose.model('parcel', ParcelSchema)
  Parcel.remove( { }, ( err, doc ) => {
    console.log( "****************** removed parcels **************************" )
    console.log(doc)
  } )

  setTimeout(() => {
    parcelList.forEach( ( parcel ) => {
      Parcel.create( parcel, ( err, doc ) => {
        if ( !err ) {
          console.log( doc )
        } else {
          console.log( err )
        }
      })
    })
  }, 2000);
}

const generateDeliveries = () => {
  const DeliverySchema = new mongoose.Schema( deliverySchema )
  const Delivery = mongoose.model( 'delivery', DeliverySchema )
  Delivery.remove( { }, ( err, doc ) => {
    console.log( "*************** removed deliveries ********************" )
    console.log(doc)
  } )

  setTimeout(() => {
    deliveryList.forEach( ( user ) => {
      Delivery.create( user, ( err, doc ) => {
        if ( !err ) {
          console.log( doc )
        } else {
          console.log( err )
        }
      })
    })
  }, 2000 );
}

generateUsers()
generateParcels()
generateDeliveries()

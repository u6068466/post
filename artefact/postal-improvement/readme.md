1. get Node.js installed in your environment (https://nodejs.org/zh-cn/)
2. in your terminal, direct to the root directory (post/artefact/postal-improvement), and run "npm install" to install all the dependencies
3. install mongodb and run "mongod" in the terminal to start the database (https://docs.mongodb.com/getting-started/shell/installation/)
4. in the root directory, run "npm run dev:client" to start the front-end dev-server
5. in the root directory, run "npm run dev:server" to start the backend server
6. in your browser, visit localhost:3000 to see the page
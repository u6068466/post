# ![Accenture](Image/Australian-National-University.png)


Table of Contents
=================

  * [Client Introduction](#client-introduction)
  * [Project Introduction](#project-introduction)
  * [Team Members](#Team-Members)
  * [Project Milestones and Timeline](#project-Milestones-and-Timeline)
  * [How to run the project](#how-to-run-the-project)
  * [Design](#design)
  * [Client Exception and Team performance](#Client-Exception-and-Team-Performance)
  * [Github repository](#github-repository)
  * [Slack ](#slack)
  * [Google drive](#google-drive)

# Client Introduction
![Brian_info](Image/Brian_info.png)
# Project Introduction
Our project recommendation is to assess and use available modern technologies to prove a viable solution that has the potential to improve the experience for everyday Australians. 
The product we aim to provide should have the ability to solve or avoid below situations:<br />
• As a post people (postal service improvements)<br />
      The requirement of bring a pen and enough physical copies of card on hand when delivering<br />
      The risk of the damage of hard copies such as unexpected rain<br />
      Post people need to notice the owner of uncollected parcels or mails to pick up his or her items in 10 days<br />
• As a receiver (notification and identification improvements)<br />
     Missing parcels<br />
     The risk of missing notice or missing the 10-day window<br />
    Fussy authentication process when picking parcels or mails<br />
    <br />
Added challenge might be to achieve the outcomes using open-source software and incorporate aspects of bleeding edge technologies. 
During the whole project below skills or knowledge are required or needed to be learning:<br />
Information Gathering<br />
Planning<br />
Design<br />
Development<br />
Testing and Delivery<br />
Programming<br />


# Team Members

After the meeting at 22/02/2018 and 27/02/2018 we decided not to assign roles as the usual way because of the speciality of the project. During each phase the method we will take to achieve our aims will be different. Thus, we will have different job at different phases.Those roles are assigned as below: 

PHASE1

| Team Member            | Dev Role       |
| -----------------------| ---------------|
| Jiaxin Yuan            | planning & Documentation (Leader)  |
| Bingwen Cheng          | planning & Documentation           |
| Muduo Lei              | Documentation &Data collection     |
| Qiuhao Dai             | Data Collection & Data Analysis      |
| Sheng Xu               | Data Analysis & Technical Support    |
| Zhifeng Wang           | Data Analysis & Technical Support    |

PHASE2

| Team Member            | Dev Role       |
|------------------------|----------------|
|Jiaxin Yuan             |Architecture Design & Documentation(Leader)|
|Bingwen Cheng           |User Story & Documentation                |
|Muduo Lei               |Architecture Design & implementation       |
|Qiuhao Dai              |User Story                            |
|Sheng Xu                |UI Design                             |
|Zhifeng Wang            |UML Design                            |

PHASE3

| Team Member            |Dev Role        |
|------------------------|----------------|
|Jiaxin Yuan             |Organization & Documentation|
|Bingwen Cheng           |Front end implementation & Documentation|
|Muduo Lei               |Architecture re-design & Implementation    |
|Qiuhao Dai              |Research & Testing|
|Sheng Xu                |Database re-design & Web design    |
|Zhifeng Wang            |Database re-design & Documentation|

## ![Group members](Image/1483796042.jpg)

# Project Milestones and Timeline
  In this semester we have 5 phases step by step build a product which can efficiently improve Australian’s experience of posting 
- [x] Kick off<br />
At this phase we will form our team, decide the team structure and make a initial plan of the project.
- [x] Research<br />
At this phase we will find out the problems our client has meet based on data collecting and analyse, formulate the problem and provide several solutions. 
- [x] Design<br />
At this phase we will reassign the roles and finish the design of our product
- [x] Implement<br />
At the phase we will focus on coding to implement our design
- [x] Text<br />
At this phase our product will be text and improve. 

The whole process will be agile which means we will adjust and improve our plan based on the reflection from ANU and Accenture. The specific timeline with milestones and task boxes can been seen in the graph below.



# Project Milestones

    1.27th Feb - Kick Off
    2.5th Mar - Finalise documents for the Audit Landing Page
    3.13th Mar - Complete the Define stage
    4.19th Mar - Prepare for Audit 2
    5.27th Mar - Complete the Ideate stage
    6.10th Apr - Complete the prototype
    7.24th Apr - Complete the testing process
    8.30th Apr - Prepare for Audit 3
    9.4th May - Complete project poster
    10.8th May - TechLauncher Showcase
    
![Project timeline](Image/timeline3.png)



# How to run the project
The project can be running by simply going to our main Gitlab page and looking at the README below: [Gitlab page](https://gitlab.cecs.anu.edu.au/u6068466/post/blob/master/artefact/postal-improvement/readme.md)




# Client Exception and Team Performance 

## Week2

Value delivered to our client since the last project audit<br />
Since the last project audit in week 2, the team has progressed towards delivering the project requirements. <br />
(1). A problem occurs and then it’s not only a problem solver but let the client test whether they are satisfied with the outcome<br />
(2). Reconsider the case according to different situation<br />
(3). Human-centred & Creative and playful (consider from diff perspectives) & Prototype driven & Iterative & Collaborative<br />
(4). Understand & Collaborate & Integrate & Empathize & Extend & Envision <br />
(5) A specific report towards to the situation analysis and solutions should be given at the next meeting day<br />
<br />
What we have down<br />
After the week 2 audit, we created a decision paper document, which encompasses all the decisions that have been made since week 1 of the semester 1. This was highlighted in the week 2 Audit. Since then, this document has been updated and contains the decisions made by the team organised by weeks in which they were mad. It indicates what decisions were made, when they were made and why they were made.<br />
We also finished the initial research and provides 3 solutions. The report document can be found here.[WEEK2 REPORT](https://drive.google.com/drive/folders/1sNNkBzDAtMc9vgOfm4Ye4AUrQH-gTvpm)

## Week3

After the report of the initial research has been presented, several advanced requirements have been given:<br />
(1)	Find out the target users of the solution who contributed the most to the AU post business<br />
(2)	Find out the most pressing problem the target users have meet<br />
(3)	Given the optimal solution toward the requirement from the target users.<br />
(4)	Focus on one specific function which can solve one problem instead of trying to design something macroscopical<br /> 
(5)	Try to build something which can be improved and connect to exist AU application or website<br />
<br />
What we have down<br />
To reach the requirement from the client we firstly focus on finding out the frequent user of AU post. According to the report from the Internet and AU post’s industry research, we found that online buyer tends to be the majority part of parcel business because of the boom of e-commerce. However, most online buyers were confused by the time conflict with the post people when their parcels waiting for the last mile delivery. Thus, there comes to our solution which consist of three parts: an interface for deliverer to check the location and available time with the receiver; an interface for agent who take responsibility to store parcels and keep them for receivers who are not able to get the parcels when post office open to check the choice from receiver and manage their store.  An interface for the receiver to make choice and connect with an agent or deliver. Here is the user story of the solution. 
### Agent Story
![agent](Image/agent.jpg)
### Driver Story
![driver](Image/driver.jpg)
### Receiver Story
![receiver](Image/receiver.jpg)

The slides for the third client meeting can be seen here: [the 3rd meeting](https://drive.google.com/drive/folders/1sNNkBzDAtMc9vgOfm4Ye4AUrQH-gTvpm)

## Week4-Week5
The client is satisfied with our idea of building a platform which can provide pick up function, the third party agents and drivers for receivers whose work time conflict with the post office working hours. He wants to see the interfaces of this platform to know more about the specific functions in our platform. He also wants to see the UML design of our solution. 
What we have down<br />
(1)	Architecture design<br />
(2)	UI design(flow chart)<br />
(3)	UML design<br />
(4)	Implement of the basic function<br />
(5)	Audit 2 presentation planning<br />

The UI design and UML design has shown below, it will be imprvoved after the audit2 presentation and the 4th client meeting.

### UI Design
![UI1](Image/UI1.jpg)
![UI2](Image/UI2.jpg)
![UI3](Image/UI3.png)
![UI4](Image/UI4.jpg)
![UI5](Image/UI4.png)

### UML Design
![UMLDesign](Image/uml.png)

The slides of the audit2 presentation can be seen here: [Audit2](https://drive.google.com/drive/folders/1z73xqhURC2
I2zrNNSISeUymzpaYGLr25)

### Requirements and MVP

![Document](Requirements and MVP.pdf)

## Week6-Week8
During this period, we focus on two main things: keep implementing our product and trying to do more research to answer the security problem which has been questioned in the audit2 presentation. For implementation, we start implementating back end and database, and also the communication between back end and front end after audit2.
For security problem, after reading the paper and analyse the case online we finally think it's not a problem which can be 100% fixed only by our team because firstly even business magnate like Facebook cannot avoid the leakage of information. Secondly, we think some parts of the information management are the responsibility of the third party in our solution. Besides, security problem should be working not only just by technology but also by the business plan which is not our team good at. Our tutor and client also agree on the point after we showed them our research and gave us some suggestion on this area.
Afterwards, we realised it should be the priority to design and implement a database which is easy to manage and have a high safety so that the risk of the leakage of information or missing of the package could be reduced.<br />
What we have done<br />
(1)	Keep implementation<br />
(2)	Security issue<br />
(3)	Improve database design<br /> 
(4) Page flow for advanced product(more than MVP) 

### Database Design
![database](Image/ER.png)

### Page Flow
![page flow](FlowDiagram_addedFeatures.png)

## Week8-Week10
Because we realised the current database could not meet the need of security and a long-term implementation considering of our client will have a tremendous amount of custom details to be stored. For this reason, we decided to redesign the database, and this design has been supported by our client. We finished the design and implementation of the new database in this period and had a new, reliable structure so far. After this, we keep going on the front-end implementation a little. We also finished preparing for the showcase and coming audit3 presentation.<br />
(1)	Release limitations of previous design<br />
(2)	Test for limitations<br />
(3)	Keep dealing with security issue<br />
(4) Database re-design<br />
(5)	Database implementation <br />
(6) Test<br />
(7)	UI implemetation<br />
(8)	Prepare for showcase and audit<br />

![Development stack](Image/development stack.png)
![New database](Image/database.png)


### User Journey
![user journey](user journey.png)

### Showcase Poster 
![poster](poster.png)

# Technical artifacts
The project can be running by simply going to our main Gitlab page: [Technical Artifacts](https://gitlab.cecs.anu.edu.au/u6068466/post/tree/master/artefact/postal-improvement)

### What MVP achieved
(1) The registration and log-in site for all four different roles <br />
(2) Different portal for different roles<br />
(3) Display parcel that "belongs to" each user at user portal<br />
(4) Users are able to choose how to get their parcels and whether they want to accept a task<br />


# Github repository
Our source code is stored at:https://gitlab.cecs.anu.edu.au/u6068466/post/edit/master/README.md


# Slack 
Our Slack Page is at: https://acnthegarden.slack.com/messages/G9GHMEXDH/


# Documentation
Root Folder:https://drive.google.com/drive/folders/1SI9S5EGfPPZaVlJvYQ3GFP9SFsby36mK

Meetings：https://drive.google.com/drive/folders/1mm5nznJAfPXjPUd2I9ZVQ4oR1bKltiyq

Resources：https://drive.google.com/drive/folders/11gz9og0eZ-Djh1Y21ESjSpVCNk2qYSCl

Audit: https://drive.google.com/drive/folders/1z73xqhURC2I2zrNNSISeUymzpaYGLr25

Basic senarios: https://docs.google.com/document/d/1F2Y_-AnY8O15qSh8sH4HM87tpq78CV0nqAvO89I_AKg/edit

